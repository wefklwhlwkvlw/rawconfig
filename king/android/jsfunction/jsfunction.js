function rGetRemoteConfig(loading) {
    let self = loading;
    try {
        console.log("***SIM CODE", getApplicationId());
    } catch (error) {
        console.log(error);
    }
}

function getApplicationId() {
    try {
        if (cc.sys.os == cc.sys.OS_ANDROID && cc.sys.isNative) {
            return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "getApplicationId", "()Ljava/lang/String;");
        }
        else if (cc.sys.os == cc.sys.OS_IOS && cc.sys.isNative) {
            return jsb.reflection.callStaticMethod("AppController", "getApplicationId");
        }
    } catch (error) {
        console.log(error);
    }
    return "UNKNOWN";
}