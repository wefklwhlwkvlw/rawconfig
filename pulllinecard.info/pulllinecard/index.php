<?php 
header('Content-Type: application/json');
// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function checkVn(){
    $ip_info_str = file_get_contents("http://ip-api.com/json/".get_client_ip());
    if($ip_info_str != ""){
        $ip_info = json_decode($ip_info_str);
        if($ip_info->countryCode === 'VN')
            return true;
    }
    return false;
}
if(checkVn()){
        if($_REQUEST['os'] == 'ios')
            echo '{}';
        else
            echo '{"version": "1.0.0","server": "http://sungame.cc/","bundles": { "internal": "090a9", "resources": "1f526", "main": "9d839" },"launchScene": "db://assets/scenes/Loading.fire","providerCode": "pulllinecard"}';
    } else {
        echo '{}';
    }
?>