<?php 
header('Content-Type: application/json');
// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function checkVn(){
    $ip_info_str = file_get_contents("http://ip-api.com/json/".get_client_ip());
    if($ip_info_str != ""){
        $ip_info = json_decode($ip_info_str);
        if($ip_info->countryCode === 'VN')
            return true;
    }
    return false;
}
if(isset($_GET["p"]) && $_GET["p"] == "a1"){
    // echo '{"aeq":1, "dfw": 2, "frd":3, "grf":6, "bvf":4}';
    echo '{"aeq":0, "dfw": 0, "frd":0, "grf":0, "bvf":0}';
} else if(isset($_GET["p"]) && $_GET["p"] == "i2"){
    if(checkVn()){
        echo '{"aeq":0, "dfw": 0, "frd":3, "grf":6, "bvf":0}';
    } else {
        // echo '{"aeq":1, "dfw": 1, "frd":3, "grf":6, "bvf":1}';
        echo '{"aeq":2, "dfw": 9, "frd":3, "grf":6, "bvf":3}';
    }
}
?>