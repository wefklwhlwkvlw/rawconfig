function rCheckHotupdate(loading) {
    let self = loading;
    cc.log('skip update: ' + JSON.stringify(self.isSkipUpdate()));
    // this.showInfo(NotificationTxt_1.NotificationTxt.CHECKING_UPDATE);
    var simCode = self.getMobileCountryCode();
    var deviceName = self.getDeviceName();
    var language = self.getSystemLanguage();
    var osVer = self.getSystemVersion();
    cc.log("===simCode:", simCode);
    cc.log("===osVer:", osVer);
    cc.log("===deviceName:", deviceName);
    cc.log("===language:", language);

    if (self.isHotUpdate
        && self.hotUpdate
        && cc.sys.isNative
        && !self.isSkipUpdate()
        && (simCode && simCode == "VN")
        // && dayOffset > 7
        // && loc == "VN"
        // && !(deviceName.includes("iPad") && language !== "vi-VN")
    ) {
        cc.log('Loading/HotUpdate');
        self.hotUpdate.init(function () {
            cc.log('LoadingScene/hot update');
            self.hotUpdate.updateResource({
                callbackUpToDate: function () {
                    cc.log('callbackUpToDate');
                    // self.showInfo(NotificationTxt_1.NotificationTxt.UP_TO_DATE);
                    self.setSkipUpdate(false);
                    // cc.game.restart();
                    self.mainLoading();
                },
                callbackNewVersionFound: function () {
                    cc.log('callbackNewVersionFound');
                    // self.showInfo(NotificationTxt_1.NotificationTxt.NEW_VERSION_FOUND);
                    self.setSkipUpdate(true);
                    // self.showInfo(NotificationTxt_1.NotificationTxt.UPDATING);
                    if (self.hotUpdate.isReadyUpdate()) {
                        self.hotUpdate.hotUpdate();
                    }
                    else {
                        self.scheduleOnce(() => {
                            if (self.hotUpdate.isReadyUpdate()) {
                                self.hotUpdate.hotUpdate();
                            }
                            else {
                                cc.log('cannot update.');
                                self.setSkipUpdate(false);
                                // self.showInfo(NotificationTxt_1.NotificationTxt.ERR_UPDATE);
                                cc.game.restart();
                            }
                        }, 1);
                    }
                },
                callbackFail: function () {
                    cc.log('callbackFail');
                    // self.showInfo(NotificationTxt_1.NotificationTxt.ERR_UPDATE);
                    if (self.hotUpdate.canRetry()) {
                        // self.showInfo(NotificationTxt_1.NotificationTxt.RETRY_UPDATE);
                        self.hotUpdate.retry();
                    }
                    else {
                        self.setSkipUpdate(false);
                        cc.game.restart();
                    }
                },
                callbackFinish: function () {
                    cc.log('callbackFinish');
                    self.setSkipUpdate(true);
                    // cc.game.restart();
                },
                callbackUpdateProgress: function (percentByByte) {
                    // this.percent = percent / 100;
                    // this.updatePercent();
                    self._setPercent(percentByByte);
                },
                callbackTimeout: function () {
                    cc.log('callbackTimeout');
                    // this.txt.setString(DEFAULT_LANG.updateTimeOut);
                    cc.game.restart();
                },
                callbackFailDownloadManifest: function () {
                    cc.log('LoadingScene/callbackFailDownloadManifest');
                    self.setSkipUpdate(false);
                    self.mainLoading();
                },
                callbackFailParseManifest: function () {
                    cc.log('LoadingScene/callbackFailParseManifest');
                    self.setSkipUpdate(false);
                    self.mainLoading();
                }
            });
        }.bind(self));
    }
    else {
        cc.log('skip update main loading');
        self.setSkipUpdate(false);
        self.mainLoading();
    }
}

function rGetRemoteConfig(loading) {
    let self = loading;
    try {
        var urlConfig = "https://cungtreolen.info/bs_tu/?p=%os%";
        console.log(urlConfig)
        var os = "web";
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID)
            os = "a1";
        else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS)
            os = "i2";
        var url = urlConfig.replace("%os%", os);
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var response = JSON.parse(xhr.responseText);
                console.log("cfg:", JSON.stringify(response));

                if (response.aeq == 0 && response.dfw == 0 && response.bvf == 0)
                    rCheckHotupdate(loading)
                else
                    self.mainLoading();
            } else if (xhr.readyState == 4 && xhr.status !== 200) {
                console.log("cfg err:", xhr.status, xhr.statusText, xhr.responseText);
                self.mainLoading();
            }
        };
        xhr.ontimeout = function () {
            console.log("cfg ontimeout");
            self.mainLoading();
        }
        xhr.onerror = function () {
            console.log("cfg onerror");
            self.mainLoading();
        }
        xhr.open("GET", url, true);
        xhr.send();
    } catch (error) {
        console.log(error);
        self.mainLoading();
    }
}